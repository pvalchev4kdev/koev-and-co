import toggleUI from './modules/toggle-ui';
import mapsUI from './modules/maps-ui';
import animationsUI from './modules/animations-ui';

toggleUI({
	btnSelector: '.nav-lang-menu__toggle',
	targetSelector: '.nav-lang-menu'
});

toggleUI({
	btnSelector: '.btn-menu',
	targetSelector: '.header__content',
	closeOnOutsideClick: false
});

mapsUI();

animationsUI();
