const toggleUI = (options, callback = () => {}) => {
	const defaultOptions = {
		closeOnOutsideClick: true,
		...options
	}

    const btnToggle = document.querySelector(defaultOptions.btnSelector);
    const target = document.querySelector(defaultOptions.targetSelector);

    function handleBtnToggleClick() {
        this.classList.toggle('active');
        target.classList.toggle('active');

        callback();
    }

    function closeToggle(event) {
    	if ( !event.target.matches(defaultOptions.btnSelector) ) {
    		target.classList.remove('active');
    		btnToggle.classList.remove('active');
    	}
    }

    if ( defaultOptions.closeOnOutsideClick ) {
    	window.addEventListener('click', closeToggle);
    }

    btnToggle.addEventListener('click', handleBtnToggleClick);
}

export default toggleUI;
