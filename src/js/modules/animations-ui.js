import AOS from 'aos';

const initAnimations = () => {
	AOS.init({
		easing: 'ease-in-cubic',
		offset: 50,
		once: true
	});
};

export default function animationsUI() {
	initAnimations();
}
