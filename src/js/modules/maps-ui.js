const styles = [
    {
        "featureType": "all",
        "stylers": [
            {
                "saturation": 0
            },
            {
                "hue": "#e7ecf0"
            }
        ]
    },
    {
        "featureType": "road",
        "stylers": [
            {
                "saturation": -70
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": -60
            }
        ]
    }
]

const mapContainer = document.getElementById('map');

const initMap = () => {
	const lat = Number(mapContainer.dataset.lat);
	const lng = Number(mapContainer.dataset.lng);

	const map = new google.maps.Map(mapContainer, {
		center: { lat, lng },
		zoom: 17,
		styles,
	});

	const position = new google.maps.LatLng(lat, lng);

	const pinMarker = {
		url: '../images/pin.svg',
		size: new google.maps.Size(64, 85),
		scaledSize: new google.maps.Size(64, 85)
	}

	const marker = new google.maps.Marker({
		position,
		map,
		icon: pinMarker
	});
};

export default function mapsUI() {
	if ( mapContainer ) {
		initMap();
	}
}
