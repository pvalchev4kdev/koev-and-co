## Dependencies
1. Latest version of [NodeJS](http://nodejs.org/)
2. Latest version of one of the following package managers

- [NPM](https://www.npmjs.com/)
- [Yarn](https://yarnpkg.com/)

## Install
In the root directory of the project run:

```
npm install
```

or

```
yarn install
```

## Development
To start the project in development mode, run:

```
npm run watch
```

or

```
yarn watch
```

## Build
To build the project, run:

```
npm run build
```

or

```
yarn build
```

This command will optimize the css and javascript in the `dist` folder.
