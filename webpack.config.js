const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: [
        path.resolve('./src/js/functions.js'),
        path.resolve('./src/sass/style.scss')
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'functions.bundle.js'
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                    compact: true
                }
            },
            {
                exclude: /node_modules/,
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
            	test: /\.(png|svg|jpg|gif)$/,
            	use: [
            		'file-loader'
            	]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style.bundle.css'
        }),
        
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),

        new BrowserSyncPlugin({
		    host: 'localhost',
		    server: {
		        baseDir: ['.']
		    }
		})
    ],
    optimization: {
        minimizer: [new UglifyJsPlugin({
            test: /\.js$/
        })]
    }
};
